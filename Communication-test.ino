#include "Communication-test.h"

MyComm* COMM;
BluetoothSerial BtSerial;
HardwareSerial HwSerial(0);

UART uart(&HwSerial);
BT bt(BtSerial);

void setup() {
  HwSerial.begin(115200);
  BtSerial.begin("Laser");

  while(1)
  {        
    if(HwSerial.available())
    {
      HwSerial.println("DEBUG HW Serial available");
      COMM = &uart;
      break;
    }
    else if (BtSerial.available()) {
      BtSerial.println("DEBUG BT Serial available");
      COMM = &bt;
      break;
    }       
  }  
  
}

void loop() {
  
  delay(500);
  COMM ->println("Example string");
}
