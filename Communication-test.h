#ifndef __COMM__
#define __COMM__

#include "BluetoothSerial.h"
#include "HardwareSerial.h"

class MyComm
{
  public:
    MyComm();
    ~MyComm();
    virtual void println(String) = 0;
};

class UART: public MyComm
{
  private:
    HardwareSerial* HwSerial;
  public:
    UART(HardwareSerial*);
    ~UART();
    void println(String);
};

class BT: public MyComm
{
  private:
    BluetoothSerial BtSerial;
  public:
    BT(BluetoothSerial);
    ~BT();
    void println(String);
};



#endif //__COMM__
