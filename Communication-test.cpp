#include "Communication-test.h"

MyComm::MyComm(){}
MyComm::~MyComm(){}

UART::UART(HardwareSerial* HwSerial): HwSerial(HwSerial){}
UART::~UART(){}
void UART::println(String msg) { HwSerial->println(msg); }

BT::BT(BluetoothSerial BtSerial): BtSerial(BtSerial){}
BT::~BT(){}
void BT::println(String msg) { BtSerial.println(msg); }
